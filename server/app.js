import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import indexRouter from './routes/index';
import api_v1 from './routes/v1/index';
import usersRouter from './routes/users';
import container from "./di/container";
// import NDIMiddleware from 'node-dependency-injection-express-middleware'

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

// const options = {
//     serviceFilePath: 'config.yml'
// };
//
// app.use(new NDIMiddleware(options).middleware());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api/v1', api_v1)
app.container = container

export default app;

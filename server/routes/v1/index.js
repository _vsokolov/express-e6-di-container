import express from 'express';
import app from '../../app'
import UserService from "../../services/UserService";
import UserRepository from "../../repositories/UserRepository";


var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    let container = app.container;
    // let x = container.get('service.user').findAll();
    let response = container.get('user_service').findAll();
    // let response = new UserService(new UserRepository());
    // let response = 11111;
    res.send(response, 200);
});

export default router;

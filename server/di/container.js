import {ContainerBuilder, Reference} from "node-dependency-injection";
import UserRepository from "../repositories/UserRepository";
import UserService from "../services/UserService";

let container = new ContainerBuilder()

container.register('user_repository', UserRepository)

container.register('user_service', UserService)
    .addArgument(new Reference('user_repository'))

container.compile()

export default container
